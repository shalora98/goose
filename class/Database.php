<?php

class Database{
    private $link;
    private $host = 'localhost';
    private $database = '';
    private $user = '';
    private $password = '';

    public function __construct(){
        $this->link = new mysqli($this->host, $this->user, $this->password, $this->database); // подключаемся к бд
        $this->link->set_charset("utf8");
    }

    public function createTable($name){ // Создаёт таблицу, если её ещё нет (с полем id);
        if($this->checkTable($name)) $this->link->query("CREATE TABLE `".$this->database."`.`".$name."` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;");
        return $this;
    }

    public function addColumn($table, $array){ // Добавляет колонку в базу данных, если её нет. $table = имя таблицы.
        $adding = true; // Разрешает или запрещает добавление колонки в таблицу
        // Принимает array( 'name' => 'price', 'param' => 'VARCHAR(256)' )
        // VARCHAR(256), JSON, INT, INT(11), TEXT, BOOLEAN, DATETIME, ...
        $result = $this->link->query("DESCRIBE ".$table.";");
        while($data[] = $result->fetch_assoc());
        foreach($data as $value){
            if($value["Field"] = $array["name"]) break; $adding = false;
        }
        if($adding) $this->link->query("ALTER TABLE `".$table."` ADD `".$array["name"]."` ".$array["param"]." NOT NULL;");
        return $this;
    }


    public function addProducts($table, $article, $title, $price){
        // if($price == '-') $price = 0; // Супер костыль!
        $result = $this->link->query("SELECT `article` FROM `".$table."` WHERE `article` = '".$article."';");
        if(!(int)$result->num_rows){
            $this->link->query("INSERT INTO `".$table."` (`id`, `article`, `title`, `price`) VALUES (NULL, '".$article."', '".$title."', '".$price."')");
            if($this->link->error != '') return $this->link->error . PHP_EOL;
        }else{
            $this->link->query("UPDATE `".$table."` SET `title` = '".$title."', `price` = '".$price."' WHERE `".$table."`.`article` = '".$article."'");
            if($this->link->error != '') return $this->link->error . PHP_EOL;
        }
    }

    public function addShops($table, $url, $name, $favicon, $della_ranked, $yandex_ranked, $difference_ranked){
        $result = $this->link->query("SELECT `name` FROM `".$table."` WHERE `name` = '".$name."';");
        if(!(int)$result->num_rows){
            $this->link->query("INSERT INTO `".$table."` VALUES (NULL, '".$url."', '".$name."', '".$favicon."', '".$della_ranked."', '".$yandex_ranked."', '".$difference_ranked."')");
            if($this->link->error != '') return $this->link->error . PHP_EOL;
        }else{
            $this->link->query("UPDATE `".$table."` SET `url` = '".$url."', `favicon` = '".$favicon."', `della_ranked` = '".$della_ranked."', `yandex_ranked` = '".$yandex_ranked."', `difference_ranked` = '".$difference_ranked."' WHERE `".$table."`.`name` = '".$name."'");
            if($this->link->error != '') return $this->link->error . PHP_EOL;
        }
    }

    public function getPricingProducts($table){ // Возвращает массив article>price 
        $result = $this->link->query("SELECT `article`, `price` FROM `".$table."`;");
        while($data[] = $result->fetch_assoc());
        return $data;
    }


    public function addUrls($table,$price,$title,$url,$article,$httpcode,$difference,$update){ // Добавляет новую ссылку на левый инет-магазин
        $result = $this->link->query("SELECT `article`, `url` FROM `".$table."` WHERE `article` = '".$article."' AND `url` = '".$url."'");
        if($result->fetch_assoc()){
            // Если запись существует
            $this->link->query("UPDATE `".$table."` SET 
            `price` = '".$price."',
            `title` = '".$title."',
            `httpcode` = '".$httpcode."',
            `difference` = '".$difference."',
            `update` = '".$update."'
            WHERE `".$table."`.`article` = '".$article."'
            AND `".$table."`.`url` = '".$url."'");
            if($this->link->error != '') return $this->link->error . PHP_EOL;
        }else{
            // Если записи не существует
            $this->link->query("INSERT INTO `".$table."` (
                `id`,
                `price`,
                `title`,
                `url`,
                `article`,
                `httpcode`,
                `difference`,
                `status`,
                `update`
                ) VALUES (
                NULL,
                '".$price."',
                '".$title."',
                '".$url."',
                '".$article."',
                '".$httpcode."',
                '".$difference."',
                'тут должен быть статус какой та',
                '".$update."'
                );");
                if($this->link->error != '') return $this->link->error . PHP_EOL;

        }
    //     $result = mysqli_query($this->link, "SELECT * FROM `urls` WHERE `url` = '".$url."'");
    //     if(!mysqli_fetch_assoc($result)) mysqli_query($this->link, "INSERT INTO `urls` (`id`, `url`, `price`, `class`, `article`) VALUES (NULL, '".$url."', '".$price."', '".$class."', '".$article."')");
    }
    





    private function checkTable($name){ // проверяет наличие таблицы
        $result = $this->link->query("SHOW TABLES FROM `".$this->database."` like '".$name."';");
        while($data[] = $result->fetch_assoc());
        return ($data[0]) ? false: true;
    }

    
    


}

