<?php
class Parser{
    public $shops = array( // набор регулярок для обработки цены и названия
        'Aquavivoru' => array(
            'price' => array(
                'foreach' => array('/.*<span class="price" id="price_text_">/s','/<\/span>.*/s'),
                'last' => '/[^\d]/'
            ),
            'title' => array(
                'foreach' => array('/.*<div class="tabs-content description-text active" itemprop="description">/s', '/<ul>.*/s', '/.*<b>/s', '/<\/b>.*/s'
            ),
                'last' => '/(.*)/'
            )
        ),
        'Vanoptorgru' => array(
            'price' => array(
                'foreach' => array('/.*<span id="opu_price">/s','/<\/span>.*/s'),
                'last' => '/[^\d]/'
            ),
            'title' => array(
                'foreach' => array('/.*<div itemprop="model">/s', '/<\/div>.*/s'
            ),
                'last' => '/(.*)/'
            )
        ),
    );

    public function getHtml($shop_name,$html, $url, $article, $httpcode, $difference){
        // возвращает json массив с ценой и названием.
            $result_price = $html;
            foreach($this->shops[$shop_name]['price']['foreach'] as $value){
                $result_price = preg_replace($value, '', $result_price);
            }

            $result_title = $html;
            foreach($this->shops[$shop_name]['title']['foreach'] as $value){
                $result_title = preg_replace($value, '', $result_title);
            }

            
            $result = array(
                'price' => (int)preg_replace($this->shops[$shop_name]['price']['last'], '\\1', $result_price),
                'title' => preg_replace($this->shops[$shop_name]['title']['last'], '\\1', $result_title),
                'url' => $url,
                'article' => $article,
                'httpcode' => $httpcode,
                'difference' => $difference
            );
            return $result;
    }

    public function differencePrice($price_della, $prise_shop){ // Возвращает разницу между ценами в процентах
        $price_della = (float)$price_della;
        $prise_shop = (float)$prise_shop;
        if($price_della == 0){
            return 100;
        }else{
            $procent_della = $price_della/100;
            return round((100 - ((float)$prise_shop / (float)$procent_della)), 2);
        }
    }








    // public function __construct($shop_name, $url){ // Получает статус страницы и его содержимого
    //     $this->shop_name = $shop_name;
    //     $this->see_url = $url;
    //     $this->url = curl_init($url);
    //     curl_setopt($this->url,  CURLOPT_RETURNTRANSFER, TRUE);
    //     $this->response = curl_exec($this->url); // контент страницы
    //     $this->httpCode = curl_getinfo($this->url, CURLINFO_HTTP_CODE); // статус сервера
    //     curl_close($this->url);
    // }

    // public function getPrice(){ // Получает ценну
    //     if($this->httpCode == 200){
    //         $result = $this->response;
    //         foreach($this->shops[$this->shop_name]['price']['foreach'] as $value){
    //             $result = preg_replace($value, '', $result);
    //         }
    //         return (int) preg_replace($this->shops[$this->shop_name]['price']['last'], '\\1', $result);
    //     }
    // }


    // public function getTitle(){ // Получает название с сайта
    //     if($this->httpCode == 200){
    //         $result = $this->response;
    //         foreach($this->shops[$this->shop_name]['title']['foreach'] as $value){
    //             $result = preg_replace($value, '', $result);
    //         }
    //         return preg_replace($this->shops[$this->shop_name]['title']['last'], '\\1', $result);
    //     }
    // }

    // public function getHttpCode(){
    //     return $this->httpCode;
    // }

}