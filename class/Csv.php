<?php
class Csv{
    private $file;
    public function __construct($file){
        $this->file = $file;
    }

    public function getText(){
        //return fopen($this->file);
    }

    public function getProducts(){ // Возвращает массив Products
        foreach($this->parseCsv() as $key => $value){
            if($key >= 6) $new_array['links'][] = $value;
        }
        foreach($new_array['links'] as $key => $value){
            $url['links'][] = array('article' => $value[0], 'title' => $value[1], 'price' => $value[2]);
        }
        return $url['links'];
    }

    public function getShops(){ // Возвращает массив интернет магазинов
        foreach($this->parseCsv() as $key => $value){
            if($key == 0) $value_url = $value;
            if($key == 1) $value_name = $value;
            if($key == 2) $value_della_ranked = $value;
            if($key == 3) $value_yandex_ranked = $value;
            if($key == 4) $value_difference_ranked = $value;
            if($key == 5) $value_favicon = $value;
        }
        foreach($value_url as $key => $value){
            if($key >= 3){
                if($value_favicon[$key] == '') $value_favicon[$key] = 'https://dellakeramika.ru/assets/template/katya/img/favicon.ico';
                $shop[] = array(
                    'url' => $value_url[$key],
                    'name' => $value_name[$key],
                    'favicon' => $value_favicon[$key],
                    'della_ranked' => (float)$value_della_ranked[$key],
                    'yandex_ranked' => (float)$value_yandex_ranked[$key],
                    'difference_ranked' => (float)$value_difference_ranked[$key]
                );
            }
        }
        return $shop;
    }

    public function getUrls(){
        foreach($this->parseCsv() as $key => $value){
            if($key == 0) $shop_url = $value;
            if($key == 1) $shop_name = $value;

            if($key >= 6){
                foreach($value as $key_in => $value_in){
                    if($key_in >=3){
                        if($value_in != ''){
                            $ulrs[] = array(
                                'shop_url' => $shop_url[$key_in],
                                'shop_name' => $shop_name[$key_in],
                                'article' => $value[0],
                                'url' => $value_in
                            );
                        }
                    }
                }

            }
        }

        return $ulrs;
    }

    private function parseCsv(){ // Парсит CSV в массив и возвращает его
        if (($fp = fopen($this->file, "r")) !== FALSE) {
            while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
                $list[] = $data;
            }
            fclose($fp);
            return $list;
        }
    }
}