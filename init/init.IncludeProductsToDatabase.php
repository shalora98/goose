<?php

require_once '../class/Database.php';
require_once '../class/Csv.php';


$database = new Database; // создаем объект класса для взаимодействия с базой данных
$products = new Csv('../file.csv'); // создаём объект класса


foreach($products->getProducts() as $value){ // записывает инфу о товарах в бд
    echo $database->addProducts('products', $value['article'], $value['title'], $value['price']);
}
echo 'Запись товаров Della в \'products\' завершена ...' .PHP_EOL;