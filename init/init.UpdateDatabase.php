<?php

require_once '../class/Database.php';

$database = new Database; // создаем класс для взаимодействия с базой данных

    $database->createTable('urls') // Создаём таблицу urls
        ->addColumn('urls',array('name'=>'url','param'=>'VARCHAR(256)'))
        ->addColumn('urls',array('name'=>'price','param'=>'FLOAT(11)'))
        ->addColumn('urls',array('name'=>'article','param'=>'VARCHAR(30)'))
        ->addColumn('urls',array('name'=>'httpcode','param'=>'INT(5)'))
        ->addColumn('urls',array('name'=>'title','param'=>'VARCHAR(256)'))
        ->addColumn('urls',array('name'=>'status','param'=>'VARCHAR(256)'))
        ->addColumn('urls',array('name'=>'difference','param'=>'FLOAT(11)'))
        ->addColumn('urls',array('name'=>'update','param'=>'INT(55)'));

        
    $database->createTable('products') // Создаём таблицу products
        ->addColumn('products',array('name'=>'article','param'=>'VARCHAR(30)'))
        ->addColumn('products',array('name'=>'title','param'=>'VARCHAR(256)'))
        ->addColumn('products',array('name'=>'price','param'=>'FLOAT(11)'));

    $database->createTable('shops') // Создаём таблицу shops
        ->addColumn('shops',array('name'=>'url','param'=>'VARCHAR(30)'))
        ->addColumn('shops',array('name'=>'name','param'=>'VARCHAR(30)'))
        ->addColumn('shops',array('name'=>'favicon','param'=>'VARCHAR(256)'))
        ->addColumn('shops',array('name'=>'della_ranked','param'=>'FLOAT(10)')) // Популярность по нашему мнению
        ->addColumn('shops',array('name'=>'yandex_ranked','param'=>'FLOAT(10)')) // Популярность по мнению яндекса
        ->addColumn('shops',array('name'=>'difference_ranked','param'=>'FLOAT(10)')); // Как сильно рейтинг della перебивает рейтинг яндекса
