<?php

require_once '../class/Database.php';
require_once '../class/Csv.php';

$database = new Database; // создаем объект класса для взаимодействия с базой данных
$shops = new Csv('../file.csv'); // создаём объект класса


foreach($shops->getShops() as $value){ // записывает инфу о интернет магазинах в бд
    echo $database->addShops('shops', $value['url'], $value['name'], $value['favicon'], $value['della_ranked'], $value['yandex_ranked'], $value['difference_ranked']);
}
echo 'Запись информации о интернет-магазинах в \'shops\' завершена ...' .PHP_EOL;